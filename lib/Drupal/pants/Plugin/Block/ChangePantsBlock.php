<?php

/**
 * @file
 * Contains \Drupal\pants\Plugin\Block\ChangePantsBlock.
 */

namespace Drupal\pants\Plugin\Block;

use Drupal\block\BlockBase;

/**
 * Provides a 'Change pants' block.
 *
 * @Block(
 *   id = "pants_change",
 *   admin_label = @Translation("Change pants")
 * )
 */
class ChangePantsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function settings() {
    return array(
      'cache' => DRUPAL_NO_CACHE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $account = \Drupal::currentUser();
    $user = user_load($account->id());

    return array(
      'status' => array(
        '#theme' => 'pants_status',
        '#pants_status' => $user->pants_status->value,
        '#prefix' => '<div id="pants-change-pants-status">',
        '#suffix' => '</div>',
      ),
      'change_link' => array(
        '#type' => 'link',
        '#title' => t('Change'),
        '#href' => "pants/change/{$user->id()}",
        '#ajax' => array(
          'wrapper' => 'pants-change-pants-status',
          'method' => 'html',
          'effect' => 'fade',
        ),
      ),
    );
  }

}
