<?php

/**
 * @file
 * Contains \Drupal\pants\Form\PantsSettingsForm.
 */

namespace Drupal\pants\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Cache\Cache;

/**
 * Configure pants settings for this site.
 */
class PantsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'pants_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->config('pants.settings');
    $form['pants_type'] = array(
      '#type' => 'radios',
      '#title' => t('Pants type'),
      '#options' => array(
        '' => t('None (just show on/off status)'),
      ),
      '#default_value' => $config->get('pants_type'),
      '#description' => t('Choose pants type to show on the user profile.'),
    );

    $pants_type_manager = \Drupal::service('plugin.manager.pants.pants_type');
    foreach($pants_type_manager->getDefinitions() as $id => $definition) {
      $form['pants_type']['#options'][$id] = $definition['label'];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $config = $this->config('pants.settings');
    $config->set('pants_type', $form_state['values']['pants_type']);
    $config->save();
    parent::submitForm($form, $form_state);

    // @todo Decouple from form: http://drupal.org/node/2040135.
    Cache::invalidateTags(array('config' => 'pants.settings'));
  }

}
