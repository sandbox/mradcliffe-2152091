<?php

/**
 * @file
 * Definition of Drupal\pants\Tests\PantsUITest.
 */

namespace Drupal\pants\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Cache\Cache;

/**
 * Tests UI of Pants module.
 */
class PantsUITest extends WebTestBase {

  protected $profile = 'testing';
  public static $modules = array('pants', 'views', 'block');

  /**
   * Standard test user.
   */
  protected $web_user;

  /**
   * An admin user.
   */
  protected $admin_user;

  public static function getInfo() {
    return array(
      'name' => 'UI on your pants',
      'description' => "Ensures that you can set and get a user's pants status through the UI.",
      'group' => 'Pants',
    );
  }

  function setUp() {
    parent::setUp();

    $this->web_user   = $this->drupalCreateUser(array('change pants status'));
    $this->admin_user = $this->drupalCreateUser(array('administer blocks', 'administer pants', 'change pants status'));
  }

  /**
   * Ensures UI functionality is working.
   */
  function testPantsUserProfileUI() {
    $this->drupalLogin($this->web_user);
    $uid = $this->web_user->id();

    // Save the user profile, so that pants status is initialized and check that
    // it's initialized to Off.
    $this->drupalPostForm("user/$uid/edit", array(), t('Save'));
    $this->drupalGet("user/$uid");
    $this->assertText('Off');

    // Change pants status and check it again.
    $edit = array('pants_status' => 1);
    $this->drupalPostForm("user/$uid/edit", $edit, t('Save'));
    $this->drupalGet("user/$uid");
    $this->assertText('On');

    // Set a non-default pants type. Ensure it is shown as an image.
    // @todo Fix config system to not require explicit cache invalidation by
    //   every caller that changes config outside of form submissions:
    //   http://drupal.org/node/2040135.
    \Drupal::config('pants.settings')->set('pants_type', 'bellbottoms')->save();
    Cache::invalidateTags(array('config' => 'pants.settings'));
    $this->drupalGet("user/$uid");
    $this->assertRaw('<img src="http://ecx.images-amazon.com/images/I/41xXmNdZn8L._SY200_.jpg"');
  }

  /**
   * Test pants block functionality.
   */
  function testPantsBlocks() {
    $this->drupalLogin($this->admin_user);

    // Turn on the pants blocks.
    $this->drupalPlaceBlock('pants_change');
    $this->drupalPlaceBlock('views_block:pants_recent-block_1');

    // Change pants status from the "Change pants" block.
    $this->drupalGet('');
    $this->clickLink(t('Change'));

    // Verify it was changed in the "Recent pants" block.
    $this->drupalGet('');
    $this->assertText('put pants on');

    // Repeat for pants off.
    $this->clickLink(t('Change'));
    $this->drupalGet('');
    $this->assertText('took pants off');
  }
}
