<?php

/**
 * @file
 * Definition of Drupal\pants\Tests\PantsAJAXTest.
 */

namespace Drupal\pants\Tests;

use Drupal\system\Tests\Ajax\AjaxTestBase;

/**
 * Tests AJAX functionality of Pants module.
 */
class PantsAJAXTest extends AjaxTestBase {

  protected $profile = 'testing';
  public static $modules = array('pants');

  /**
   * Standard test user.
   */
  protected $web_user;

  public static function getInfo() {
    return array(
      'name' => 'AJAX on your pants',
      'description' => "Ensures that you can set and get a user's pants status through AJAX.",
      'group' => 'Pants',
    );
  }

  function setUp() {
    parent::setUp();

    $this->web_user   = $this->drupalCreateUser(array('change pants status'));
  }

  /**
   * Ensures AJAX functionality is working.
   */
  function testPantsAJAX() {
    $this->drupalLogin($this->web_user);

    // Send an AJAX request and verify the expected command is returned.
    $commands = $this->drupalGetAJAX('pants/change/' . $this->web_user->id());
    $expected_command = array(
      'command' => 'insert',
      'data' => 'On',
    );
    $this->assertCommand($commands, $expected_command, 'Expected AJAX command returned in response to putting pants on.');

    // Repeat for pants off.
    $commands = $this->drupalGetAJAX('pants/change/' . $this->web_user->id());
    $expected_command = array(
      'command' => 'insert',
      'data' => 'Off',
    );
    $this->assertCommand($commands, $expected_command, 'Expected AJAX command returned in response to taking pants off.');
  }
}
