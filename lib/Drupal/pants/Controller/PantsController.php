<?php

/**
 * @file
 * Contains \Drupal\pants\Controller\PantsController.
 */

namespace Drupal\pants\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;

/**
 * Returns responses for Pants routes.
 */
class PantsController extends ControllerBase {

  /**
   * Changes pants status and returns the display of the new status.
   *
   * @param \Drupal\user\UserInterface $user
   *   A user entity.
   */
  function change(UserInterface $user) {
    $current_pants_status = $user->pants_status->value;
    $new_pants_status = 1 - $current_pants_status;
    $user->pants_status->value = $new_pants_status;
    $user->save();
    return array(
      '#theme' => 'pants_status',
      '#pants_status' => $new_pants_status,
    );
  }

}
