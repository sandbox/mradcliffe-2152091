<?php

/**
 * @file
 * Contains \Drupal\pants\PantsTypeManager.
 */

namespace Drupal\pants;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages pants type plugins.
 */
class PantsTypeManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/PantsType', $namespaces, 'Drupal\pants\Annotation\PantsType');

    $this->alterInfo($module_handler, 'pants_type_info');
    $this->setCacheBackend($cache_backend, $language_manager, 'pants_type');
  }

}
