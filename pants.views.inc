<?php

/**
 * @file
 * Provide views data for pants.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function pants_views_data() {
  $data['pants_history']['table']['group'] = t('Pants history');
  $data['pants_history']['table']['base'] = array(
    'field' => 'changed',
    'title' => t('Pants history'),
  );

  // Describe the pants_history.uid field. This was initially copied from
  // $data['node']['uid'] within node_views_data(), and then changed as needed.
  $data['pants_history']['uid'] = array(
    'title' => t('Pants wearer uid'),
    'help' => t('The user whose pants were taken on/off. If you need more fields than the uid, add the Pants wearer relationship.'),
    'relationship' => array(
      'title' => t('Pants wearer'),
      'help' => t('Relate statuses to the user who is/is not wearing pants.'),
      'id' => 'standard',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('Pants wearer'),
    ),
    'filter' => array(
      'id' => 'user_name',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'field' => array(
      'id' => 'user',
    ),
  );

  // Describe the pants_history.status field. This was initially copied from
  // $data['node']['status'] within node_views_data(), and then changed as
  // needed.
  $data['pants_history']['status'] = array(
    'title' => t('Pantsed'),
    'help' => t('Whether or not the user is currently wearing pants.'),
    'field' => array(
      'id' => 'boolean',
    ),
    'filter' => array(
      'id' => 'boolean',
      'label' => t('Pantsed'),
      'type' => 'yes-no',
      // Use status = 1 instead of status <> 0 in WHERE statement.
      'use_equal' => TRUE,
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  // Describe the pants_history.changed field. This was initially copied from
  // $data['node']['changed'] within node_views_data(), and then changed as
  // needed.
  $data['pants_history']['changed'] = array(
    'title' => t('Changed date'),
    'help' => t('The date the pants were taken on/off.'),
    'field' => array(
      'id' => 'date',
    ),
    'sort' => array(
      'id' => 'date'
    ),
    'filter' => array(
      'id' => 'date',
    ),
  );

  // Describe the pants_history.changed_by field. This was initially copied from
  // $data['node']['uid'] within node_views_data(), and then changed as needed.
  $data['pants_history']['changed_by'] = array(
    'title' => t('Pants changer uid'),
    'help' => t('The user whose who did the taking on/off of the pants. If you need more fields than the uid, add the Pants changer relationship.'),
    'relationship' => array(
      'title' => t('Pants changer'),
      'help' => t('Relate statuses to the user who changed it.'),
      'id' => 'standard',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('Pants changer'),
    ),
    'filter' => array(
      'id' => 'user_name',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'field' => array(
      'id' => 'user',
    ),
  );

  return $data;
}
